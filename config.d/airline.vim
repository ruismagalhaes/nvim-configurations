"configurations for vim-airline

let g:airline_theme = 'solarized'
let g:airline_solarized_bg = 'dark'
