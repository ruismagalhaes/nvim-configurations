"Default nvim configs
set number
set ruler
syntax on
set termguicolors
set splitbelow
set splitright
set nocompatible

"set theme to use
colorscheme gruvbox

" ftplugin on
filetype plugin indent on

"Ensure that, once function is written, completion window disappears
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

"Make sure lua signature is loaded on every file
autocmd BufRead,BufNewFile * lua require"lsp_signature".on_attach()
autocmd BufReadPre,FileReadPre * lua require"lsp_signature".on_attach()

"make tab 4 spaces for go files
au BufNewFile,BufRead *.go setlocal noet ts=4 sw=4 sts=4

"Makes possible to navigate through the auto-completion list with tab
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

"fix the not yanked area highlight problem for some themes
hi HighlightedyankRegion cterm=reverse gui=reverse

" set highlight duration time to 2000 ms, i.e., 2 second
let g:highlightedyank_highlight_duration = 2000

"Go fmt automatically on save

function! CustomGoFmt()
  let file = expand('%')
  silent execute "!gofmt -w " . file
  edit!
endfunction

command! CustomGoFmt call CustomGoFmt()
augroup go_autocmd
  autocmd BufWritePost *.go CustomGoFmt
augroup END
