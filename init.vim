"calls vim-plug, a plugin manager for vim

call plug#begin('~/.local/share/nvim/plugged')
"--- PLUGINS ---

"highlight yank area
Plug 'machakann/vim-highlightedyank'

"Highlights whitespaces
Plug 'ntpeters/vim-better-whitespace'

"An arctic, north-bluish clean and elegant nvim color theme"
Plug 'arcticicestudio/nord-vim'

"Popular Vim colorscheme
Plug 'morhetz/gruvbox'

"status bar plugin
Plug 'vim-airline/vim-airline'
"More status bar themes
Plug 'vim-airline/vim-airline-themes'

"Automatic quotes and bracket completion
Plug 'jiangmiao/auto-pairs'

"Comment plugin. Depending on the file type, the comment style changes
Plug 'scrooloose/nerdcommenter'

"File managing and exploring
Plug 'scrooloose/nerdtree'

" Terraform
Plug 'hashivim/vim-terraform'

"--- markdown-preview plugin and associated plugins
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

"--- nvim-lspconfig plugin and associated plugins
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-compe'
Plug 'kabouzeid/nvim-lspinstall'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'ray-x/lsp_signature.nvim'

" colorscheme for LSP dignostics
Plug 'folke/lsp-colors.nvim'

" plugs for lsp configs
Plug 'nvim-lua/lsp-status.nvim'
Plug 'nvim-lua/completion-nvim'
Plug 'glepnir/lspsaga.nvim'
Plug 'lukas-reineke/format.nvim'

call plug#end()

lua require("")

" Load custom configurations for plugins
for config in split(globpath('~/.config/nvim/config.d', '*.vim'), '\n')
    exe 'source' config
endfor

