# nvim configurations repo

This are all my configs used on nvim for my MAC.
I'm using iTerm2.

## steps

### 1st step

```bash
# install nvim
brew install nvim
# install plug manager
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
### 2nd step

- move the "init.vim" file and the "config.d" folder to your "$HOME/.config/nvim/" path on your OS X

### 3rd step
Open a file with nvim and run ":PlugInstall"

### 4th step
Install needed packages with

```bash
python3 -m pip install <package_name>
```
### 5th step
Install nvim treesitter parser with 

```bash
:TSInstall <package_name>
```

### 6th step
Install/update the language server for <language>

```bash
:LspInstall <language>
```
